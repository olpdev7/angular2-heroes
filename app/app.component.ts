import {Component} from '@angular/core';

import { HeroDetailComponent } from './hero-detail.component';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
    <!--<span [style.padding]="test" [innerText]="title"></span>-->
    <h1>{{title}}</h1> 
    <nav>
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['app.component.css'],
})
export class AppComponent {
  // title: string;
}